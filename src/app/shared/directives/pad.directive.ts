import { Directive, ElementRef, HostBinding, Input, numberAttribute, Renderer2, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../components/card.component';

@Directive({
  selector: '[appPad]',
  standalone: true,
  exportAs: 'padd'
})
export class PadDirective {
  @Input()
  set appPad(val: 'sm' | 'xl') {
    // this.el.nativeElement.style.padding = val + 'px';
    this.renderer.setStyle(
      this.el.nativeElement,
      'padding',
      val === 'sm' ? `10px` : '30px'
    )
  }

  @HostBinding() get class() {
    // console.log('...')
    return ''
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private view: ViewContainerRef
  ) {

  }

  pippo = 123;
}

