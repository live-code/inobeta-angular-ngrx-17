// source ngFor:
// https://github.com/angular/angular/blob/main/packages/common/src/directives/ng_for_of.ts#L139

import { Directive, inject, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { User } from '../../features/demo9.component';

export type ContextType = {
  total: number,
  index: number,
  $implicit: User & { displayName: string}
}

@Directive({
  selector: '[appUsersRepeater]',
  standalone: true
})
export class UsersRepeaterDirective {

  constructor(
    private view: ViewContainerRef,
    private tpl: TemplateRef<ContextType>,
  ) {}

  // @Input() set appUsersRepeaterOf(data: User[]) {
  @Input() set appUsersRepeaterOf(data: User[]) {

    data.forEach((item, i ) => {
      this.view.createEmbeddedView<ContextType>(this.tpl, {
        $implicit: {
          ...item,
          displayName: item.name + ' ' + item.surname,
        },
        index: i +1 ,
        total: data.length
      } as ContextType)
    })

  }

}
