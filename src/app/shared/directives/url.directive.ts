import { Directive, ElementRef, HostBinding, HostListener, inject, Input } from '@angular/core';

@Directive({
  selector: '[appUrl]',
  standalone: true
})
export class UrlDirective {
  @HostBinding() class = 'text-orange-400 underline'

  @Input({ required: true })
  appUrl!: string

  @HostListener('click')
  openURL() {
    window.open(this.appUrl)
  }

}
