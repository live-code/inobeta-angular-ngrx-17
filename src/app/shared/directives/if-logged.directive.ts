import { Directive, effect, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinctUntilChanged } from 'rxjs';

@Directive({
  selector: '[appIfLogged]',
  standalone: true
})
export class IfLoggedDirective {

  constructor(
    private tpl: TemplateRef<any>,
    private view: ViewContainerRef
  ) {


    console.log('app if logged', this.tpl)
    view.createEmbeddedView(tpl)
    setTimeout(() => {
      view.clear()
    }, 2000)
  }

}
