import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appStopPropagation]',
  standalone: true
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  doSomething(e: MouseEvent) {
    e.stopPropagation();
  }

}
