import { Directive, ElementRef, inject, Input, OnInit, Renderer2 } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NavigationEnd, Router } from '@angular/router';

@Directive({
  selector: '[appMyRouterLinkActive]',
  standalone: true,
})
export class MyRouterLinkActiveDirective  {
  @Input({ required: true, alias: 'appMyRouterLinkActive' }) cssClass!: string;
  renderer = inject(Renderer2);
  el = inject(ElementRef<HTMLElement>);
  router = inject(Router);

  constructor() {
    this.router.events
      .pipe(
        takeUntilDestroyed()
      )
      .subscribe(ev => {
        if (ev instanceof NavigationEnd) {
          const routerLink = this.el.nativeElement.getAttribute('routerLink')
          if (routerLink && ev.url.includes(routerLink)) {
            this.renderer.addClass(
              this.el.nativeElement,
              this.cssClass
            )
          } else {
            this.renderer.removeClass(
              this.el.nativeElement,
              this.cssClass
            )
          }
        }
      })

  }

}
