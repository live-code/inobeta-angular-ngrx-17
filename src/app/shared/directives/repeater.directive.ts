import { Directive, Input, NgIterable, TemplateRef, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../components/card.component';

export type ContextType<T> = {
  total: number,
  index: number,
  $implicit: T
}

@Directive({
  selector: '[repeater]',
  standalone: true
})
export class RepeaterDirective<T, U extends NgIterable<T>> {
// export class RepeaterDirective<T> {

  @Input() set repeaterOf(data: T[]) {

    data.forEach((item, i) => {
      this.view.createEmbeddedView<ContextType<T>>(
        this.tpl,
        {
          total: data.length,
          index: i+1,
          $implicit: {
            ...item,
            // displayName: `${item.name} ${item.surname}`
          }
        }
      )
    })
  }

  constructor(
    private tpl: TemplateRef<ContextType<T>>,
    private view: ViewContainerRef
  ) {

  }
}

