import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-big-list',
  standalone: true,
  imports: [CommonModule],
  template: `
      <div class="grid grid-cols-12">
          <div
            class="bg-sky-400 rounded-xl p-2 m-1 inline-block"
            *ngFor="let item of items; let i = index">{{i}}</div>

      </div>

  `,
  styles: [`
    .item {
      @apply bg-sky-400
    }
  `]
})
export class BigListComponent {

  items = new Array(50000)
}
