import { JsonPipe } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tab-bar',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <p>
      tab-bar works! {{items | json}} 
    </p>
  `,
  styles: ``
})
export class TabBarComponent {
  @Input() items: number[] = []
}
