import { AfterContentInit, Component, ContentChildren, Input, QueryList, ViewChildren } from '@angular/core';
import { AccordionGroupComponent } from './accordion-group.component';

@Component({
  selector: 'app-accordion',
  standalone: true,
  imports: [],
  template: `
      <div class="border border-white p-3">
        <ng-content></ng-content>
      </div>
  `,
  styles: ``
})
export class AccordionComponent implements AfterContentInit {
  @Input() prefix: string = '';
  @ContentChildren(AccordionGroupComponent) groups!: QueryList<AccordionGroupComponent>;

  ngAfterContentInit() {
    this.groups.toArray()[0].opened = true;
    this.groups.toArray()
      .forEach(g => {
        g.headerClick.subscribe(() => {
          this.closeAll();
          g.opened = true;
        })
      })

  }

  closeAll() {
    this.groups.toArray()
      .forEach(g => {
        g.opened = false;
      })
  }

}
