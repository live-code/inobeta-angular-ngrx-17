import { Component, EventEmitter, inject, Input, Output, ViewContainerRef } from '@angular/core';
import { CardComponent } from './card.component';

@Component({
  selector: 'app-wizard',
  standalone: true,
  imports: [],
  template: `
    
    ????? COMPONENT DYNAMIC 
    
    <div>currentIndex: {{currentIndex}}</div>
    <button class="btn">prev</button>
    <button class="btn" (click)="inc()">next</button>
    <button class="btn">complete</button>
    
  `,
  styles: ``
})
export class WizardComponent {
  view = inject(ViewContainerRef)
  @Input() set config(data: any) {
    data.list.forEach((config: any) => {
      const compo = this.view.createComponent(CardComponent)
    })
  }

  @Input() currentIndex: number = 0;
  @Output() currentIndexChange = new EventEmitter()

  inc() {
    this.currentIndex = this.currentIndex+1
    this.currentIndexChange.emit(this.currentIndex)
  }
}
