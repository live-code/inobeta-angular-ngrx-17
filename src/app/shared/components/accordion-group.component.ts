import { NgIf } from '@angular/common';
import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { AccordionComponent } from './accordion.component';

@Component({
  selector: 'app-accordion-group',
  standalone: true,
  imports: [
    NgIf
  ],
  template: `
      <div>
          <div class="title" 
               (click)="headerClick.emit()" >
            {{parent?.prefix}} {{title}}
          </div>
          <div *ngIf="opened" class="body" >
              <ng-content></ng-content>
          </div>
      </div>
  `,
  styles: [`
    .title {
      @apply bg-slate-500 text-white p-2 cursor-pointer;
    }
    .body {
      @apply border border-slate-500 p-2 border-t-0 overflow-hidden;
    }
  `]

})
export class AccordionGroupComponent {
  @Input() title = ''
  @Input() opened = false
  @Output() headerClick = new EventEmitter()

  parent = inject(AccordionComponent, { optional: true})

  ngOnInit() {
    if (parent)
      this.parent!.prefix = 'efoewfew'
  }
}
