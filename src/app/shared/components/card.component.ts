import { booleanAttribute, Component, Input, numberAttribute, Signal } from '@angular/core';
import { User } from '../../features/demo5.component';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [],
  template: `
    <p>
      card works! {{ value  }} {{zoom}}
    </p>
  `,
})
export class CardComponent {
  @Input({
    alias: 'counter',
    transform: (val: number) => {
      return val * 5
    }})
  value = 0;

  /*@Input({required: true})
  user!: User;*/

  @Input({ transform: booleanAttribute})
  showArrow!: boolean

  @Input({ transform: numberAttribute})
  zoom!: number

  ngOnChanges() {

  }
}
