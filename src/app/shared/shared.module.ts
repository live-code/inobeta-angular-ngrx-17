import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { TabBarComponent } from './components/tab-bar.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CardComponent,
    TabBarComponent,
  ],
  exports: [
    CardComponent,
    TabBarComponent,
  ]
})
export class SharedModule { }
