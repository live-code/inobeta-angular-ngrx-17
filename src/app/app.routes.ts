import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { provideState } from '@ngrx/store';
import { filtersFeature } from './core/store/products/filters/filters.feature';
import { productsFeature } from './core/store/products/products/products.feature';


export const canActivate = () => {
  const router = inject(Router)
  const http = inject(HttpClient)

  if (true) {
    // .... ????
    router.navigateByUrl('home')
  }
  return false
}

export const routes: Routes = [
  {
    path: 'demo1',
    loadComponent: () => import(`./features/demo1.component`)
  },
  {
    path: 'demo2',
    loadComponent: () => import(`./features/demo2.component`)
  },
  {
    path: 'demo3',
    loadComponent: () => import(`./features/demo3.component`),
  },
  { path: 'demo4', loadComponent: () => import(`./features/demo4.component`), },
  { path: 'demo5', loadComponent: () => import(`./features/demo5.component`), },
  {
    path: 'demo6', loadComponent: () => import(`./features/demo6.component`),
  },
  {
    path: 'demo7-ngrx', loadComponent: () => import(`./features/demo7-ngrx.component`),
    providers: [
      provideState({ name: productsFeature.name, reducer: productsFeature.reducer }),
      provideState({ name: filtersFeature.name, reducer: filtersFeature.reducer }),
    ]
  },
  {
    path: 'demo8-ngrx-async',
    loadComponent: () => import(`./features/demo8-ngrx-async.component`),
    providers: [
      provideState({ name: productsFeature.name, reducer: productsFeature.reducer }),
      provideState({ name: filtersFeature.name, reducer: filtersFeature.reducer }),
    ]
  },
  {
    path: 'product/:productId',
    loadComponent: () => import(`./features/product.component`),
    data: {
      type: 'edit'
    },
    canActivate: [canActivate]
  },
  { path: 'demo9', loadComponent: () => import(`./features/demo9.component`), },
  { path: 'demo10-rxjs-signal', loadComponent: () => import(`./features/demo10-rxjs-signal.component`), },
  { path: 'demo11-uikit', loadComponent: () => import(`./features/demo11-uikit.component`), },

  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'demo1'
  }

];
