export type Product = {
  id: number;
  name: string;
  cost: number;
}


// export type ProductForm = Pick<Product, 'id' | 'cost'> & { cost: string }

