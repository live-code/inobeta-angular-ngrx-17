import { NgIf } from '@angular/common';
import { Component, signal } from '@angular/core';
import { CardComponent } from '../shared/components/card.component';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [
    CardComponent,
    NgIf
  ],
  template: `
    <h1>Esempio Card Component with Inputs</h1>

    <app-card
      title="one"
      [counter]="5"
      zoom="10" 
      showArrow
    />
    
    <app-card
      title="two"
      [counter]="4"
    />    
    
  `,
  styles: ``
})
export default class Demo5Component {
  counter = signal(0)

  user: User | undefined;
}

export type User = {
  id: number
}
