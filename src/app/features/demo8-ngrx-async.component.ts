import { Component, inject, OnInit } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { debounceTime } from 'rxjs';
import { CartActions } from '../core/store/cart/cart.actions';
import { FiltersActions } from '../core/store/products/filters/filters.actions';
import { selectFilteredList } from '../core/store/products/filters/filters.selectors';
import { ProductsActions } from '../core/store/products/products/products.actions';
import { selectHasError, selectList } from '../core/store/products/products/products.feature';
import { RouterActions } from '../core/store/router/router.actions';
import { selectUrl } from '../core/store/router/router.selectors';
import { Product } from '../model/product';

@Component({
  selector: 'app-demo8-ngrx-async',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <p>
      demo8-ngrx-async works!
    </p>

    @if(error()) {
        <div>ahia!!</div>
    }
    
    <input 
      type="text" placeholder="Search"
      [formControl]="input"
    >
    
    @for(product of products(); track product.id) {
        <li>
            {{product.name}} - {{product.cost}}
            <button (click)="addProduct(product)">add</button>
        </li>
    }
    
  `,
  styles: ``,
})
export default class Demo8NgrxAsyncComponent  {
  store = inject(Store)
  products = this.store.selectSignal(selectFilteredList)
  error = this.store.selectSignal(selectHasError)
  input = new FormControl('', { nonNullable: true })

  actions$ = inject(Actions)

  constructor() {
    this.store.dispatch(ProductsActions.load())

    this.actions$
      .pipe(
        ofType(ProductsActions.loadSuccess),
        takeUntilDestroyed()
      )
      .subscribe(console.log)

    this.input.valueChanges
      .pipe(
        debounceTime(300)
      )
      .subscribe(text => {
        this.store.dispatch(FiltersActions.search({ text }))
      })

  }

  addProduct(item: Product) {
    this.store.dispatch(CartActions.add({ item }))
  }
}
