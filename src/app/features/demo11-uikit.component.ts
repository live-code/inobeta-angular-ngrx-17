import { NgForOf } from '@angular/common';
import { Component, computed, QueryList, signal, ViewChild, ViewChildren } from '@angular/core';
import { AccordionGroupComponent } from '../shared/components/accordion-group.component';
import { AccordionComponent } from '../shared/components/accordion.component';
import { WizardComponent } from '../shared/components/wizard.component';

@Component({
  selector: 'app-demo11-uikit',
  standalone: true,
  imports: [
    NgForOf,
    WizardComponent,
    AccordionGroupComponent,
    AccordionComponent
  ],
  template: `
    <p>
      demo11-uikit works!
    </p>
    
    <pre>{{config.currentIndex}}</pre>
    <app-wizard 
      [config]="config" 
      [(currentIndex)]="config.currentIndex" />
    
    <app-accordion prefix="chapter: ">
      <app-accordion-group title="one">blabla</app-accordion-group>
      <app-accordion-group title="two">
          <button>click</button>
      </app-accordion-group>
      <app-accordion-group title="three">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias asperiores deleniti dignissimos exercitationem hic minus nostrum odio quis voluptas. Dolorum eaque fuga laudantium obcaecati officiis omnis perspiciatis praesentium similique.
      </app-accordion-group>
    </app-accordion>

    <app-accordion-group 
        title="ALONE" 
        [opened]="visible" 
        (headerClick)="visible = !visible"
    >
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias asperiores deleniti dignissimos exercitationem hic minus nostrum odio quis voluptas. Dolorum eaque fuga laudantium obcaecati officiis omnis perspiciatis praesentium similique.
    </app-accordion-group>
    
    <fx justify="between">
        <div></div>
        <div></div>
        <div></div>
    </fx>
    
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>
    
  `,
  styles: ``
})
export default class Demo11UikitComponent {
  visible = false;

  config = {
    list: [
      {compo: 'Step1', inputs: 123, output: ['click'] },
      {compo: 'Step1', inputs: 123, output: ['click'] },
      {compo: 'Step1', inputs: 123, output: ['click'] },
      {compo: 'Step1', inputs: 123, output: ['click'] },
    ],
    currentIndex: 0
  }
}
