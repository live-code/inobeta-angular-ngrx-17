import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [],
  template: `
    <p>
      product works! {{productId}} {{type}}
    </p>
  `,
  styles: ``
})
export default class ProductComponent {
  @Input() productId: string = ''
  @Input() type: 'edit' | 'add' | undefined
}
