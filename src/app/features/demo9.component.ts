import { NgForOf } from '@angular/common';
import { Component, ElementRef, inject, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IfLoggedDirective } from '../shared/directives/if-logged.directive';
import { PadDirective } from '../shared/directives/pad.directive';
import { RepeaterDirective } from '../shared/directives/repeater.directive';
import { StopPropagationDirective } from '../shared/directives/stop-propagation.directive';
import { UrlDirective } from '../shared/directives/url.directive';
import { UsersRepeaterDirective } from '../shared/directives/users-repeater.directive';

@Component({
  selector: 'app-demo9',
  standalone: true,
  imports: [
    PadDirective,
    FormsModule,
    UrlDirective,
    StopPropagationDirective,
    IfLoggedDirective,
    NgForOf,
    RepeaterDirective,
    UsersRepeaterDirective
  ],
  template: `

    <button
      *appIfLogged
      class="btn btn-warning"
    >
        ADMIN BUTTON
    </button>
      
    <h1 #ref="padd" appPad="xl">Directives</h1>
    
    <button
      class="btn" 
      (click)="val = val + 10">+</button>
  
    <button
      appUrl="https://www.fabiobiondi.dev"
      class="btn" 
     > visit web site</button>
  
    <span 
      appUrl="https://www.fabiobiondi.dev"
    >visit </span>
    
    <h1>{{val}}</h1>
    
    <div (click)="parent()">
        <div
          appStopPropagation
           (click)="child()">
            CHILD EXAMPLE
        </div>
    </div>
    
    
    <li *ngFor="let user of users; let i = index">
        {{i+1}} {{user.name}}
    </li>
    
    <li *appUsersRepeater="let user of users; let i = index, let t = total">
        {{i}} {{user.displayName}} - {{t}}
    </li>
    
    <li *repeater="let user of users; let i = index, let t = total">
        {{i}} {{user.displayName}} - {{t}}
    </li>
  `,
  styles: ``,
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export default class Demo9Component {
  @ViewChild('ref') el!: PadDirective;
  @ViewChild('msg') msg!: TemplateRef<any>;
  @ViewChild(PadDirective) el2!: PadDirective;

  users: User[] = [
    { id: 1, name: 'Mario', surname: 'Rossi' },
    { id: 2, name: 'Lorenzo', surname: 'Verdi' },
    { id: 3, name: 'Fabio', surname: 'Biondi' },
    { id: 4, name: 'Fabio', surname: 'Biondi' },
  ];
  val = 0

  parent() {
    console.log('parent')
  }

  child() {
    console.log('child')
  }
}

export type User = {
  id: number;
  name: string;
  surname: string
}

