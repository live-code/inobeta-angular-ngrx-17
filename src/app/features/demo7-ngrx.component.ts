import { AsyncPipe, JsonPipe } from '@angular/common';
import { Component, computed, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  decrement,
  increment,
  selectCounterState,
  selectValue,
  selectValueMultiplied
} from '../core/store/counter/counter.feature';

@Component({
  selector: 'app-demo7-ngrx',
  standalone: true,
  imports: [
    AsyncPipe,
    JsonPipe
  ],
  template: `
    <h1>Demo NGRX: Counter</h1>
    <button class="btn" (click)="inc()">+</button>
    
    <div>counter: {{counter()}}</div>
    <div>multiplier: {{multiplier()}}</div>
    <div>iszero: {{isZero()}}</div>
    
    <pre>Counter State: {{counterState() | json}}</pre>
  `,
  styles: ``
})
export default class Demo7NgrxComponent {
  store = inject(Store);

  counter = this.store.selectSignal(selectValue)
  counterState = this.store.selectSignal(selectCounterState)
  multiplier = this.store.selectSignal(selectValueMultiplied)

  isZero = computed(() => this.counter() === 0)

  dec() {
    this.store.dispatch(decrement({  value: 5 }))
  }
  inc() {
    this.store.dispatch(increment())
  }
}
