import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, computed, effect, inject, Signal, signal } from '@angular/core';

type User = {
  id: number;
  name: string;
}

@Component({
  selector: 'app-demo1',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    JsonPipe,
    NgIf
  ],
  template: `
    <h1 [style.color]="getZeroStyle()">
      Demo Signal: {{counterSignal()}}
    </h1>

    @if (isZero()) {
        <div>Counter is zero</div>
    }
    
    <button class="btn" (click)="dec()">-</button>
    <button class="btn" (click)="inc()">+</button>
    <button class="btn" (click)="reset()">reset</button>
    
    <pre>{{userSignal() | json}}</pre>

    {{ render() }}
  `,
})
export default class Demo1Component {
  http = inject(HttpClient);
  counterSignal = signal(1);
  userSignal = signal<User | null>(null)

  constructor() {
    effect(() => {
      localStorage.setItem('counterSignal', this.counterSignal().toString())
      this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${this.counterSignal()}`)
        .subscribe(res => {
          this.userSignal.set(res)
        })
    });
  }

  isZero = computed(() => {
    return this.counterSignal() === 0;
  })

  getZeroStyle = computed(() => {
    return this.counterSignal() === 0 ? 'red' : 'white';
  })

  inc() {
    this.counterSignal.set(this.counterSignal() + 1)
  }

  dec() {
    this.counterSignal.update(c => c - 1)
  }

  reset() {
    this.counterSignal.set(11)
  }

  render() {
    console.log('compo 1')
  }

}
