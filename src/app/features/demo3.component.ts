import { JsonPipe, NgForOf, NgIf } from '@angular/common';
import { Component, computed, signal } from '@angular/core';

type Product = {
  id: number;
  name: string;
  cost: number;
}

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    JsonPipe,
    NgIf,
    NgForOf
  ],
  template: `
      <h1>Todo List with Signals</h1>

      <input
        type="text" (keydown.enter)="addTodo(input)" #input
        placeholder="add todo"
      >

      @for (todo of todos(); track todo.id) {
          <div [style.text-decoration]="todo.completed ? 'line-through' : 'none'">
              <input type="checkbox" [checked]="todo.completed" (change)="toggleTodo(todo.id)">
              {{ todo.title }}

              <button (click)="removeTodo(todo.id)">remove</button>
          </div>
      }
     

      <pre>{{todos() | json}}</pre>

      <button (click)="save()">save</button>
  `,
})
export default class Demo3Component {
  todos = signal<Todo[]>([
    { id: 1, title: 'Todo 1', completed: true },
    { id: 2, title: 'Todo 2', completed: false },
    { id: 3, title: 'Todo 3', completed: true },
  ])

  addTodo(input: HTMLInputElement) {
    if (!input.value) return;

    this.todos.update(todos => [
        ...todos,
        {
          id: Date.now(),
          title: input.value,
          completed: false
        }
      ])
    input.value = ''
  }

  removeTodo(id: number) {
    this.todos.update(todos => todos.filter(todo => todo.id !== id))
  }

  toggleTodo(id: number) {
    this.todos.update(todos => {
      return todos.map(todo => {
        return todo.id === id ? {...todo, completed: !todo.completed} : todo
      })
    })
  }

  save() {
    console.log(this.todos())
  }
}
interface Todo {
  id: number;
  title: string;
  completed: boolean;
}
