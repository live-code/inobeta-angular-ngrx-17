import { Component, signal } from '@angular/core';
import { BigListComponent } from '../shared/components/big-list.component';
import { CardComponent } from '../shared/components/card.component';
import { TabBarComponent } from '../shared/components/tab-bar.component';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    CardComponent,
    TabBarComponent,
    BigListComponent
  ],
  template: `
    <h1>Defer Demo</h1>

    @defer (on viewport(trigger)) {
         <app-card title="ciao"/>
    } @placeholder {
        <div>...</div>
    }

    @defer (on timer(2000)) {
        <app-tab-bar />
    }

    @defer (on interaction) {
      <app-big-list />
    } @placeholder {
      <button>load big list</button>   
    } @loading {
        <div>loading...</div>
    } @error {
        failed to load
    }
    
    <div style="height: 1000px"></div>
    
    <footer style="display: none" #trigger>footer</footer>  
    
  `,
})
export default class Demo4Component {

}
