import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [],
  template: `
      <h1>Demo if block</h1>
      @if (logged()) {
          <h1>hi</h1>
          <button class="btn" (click)="logout()">logout</button>
      } @else {
          <h2>Login</h2>
          <button class="btn" (click)="signIn()">Sign in</button>
      }
      <h1>Demo switch block</h1>

      @switch (currentStep()) {
          @case ('step1') {
            <h1>Step 1</h1>
            <button class="btn" (click)="currentStep.set('step2')">NEXT</button>
          }
          @case ('step2') {
              <h1>Step 2</h1>
              <button class="btn" (click)="currentStep.set('step3')">NEXT</button>
          }
          @case ('step3') {
          }
          @default {
              <h1>HI</h1>
              <button class="btn" (click)="currentStep.set('step1')">NEXT</button>
          }
      }
      
      {{render()}}
  `,
  styles: ``
})
export default class Demo2Component {
  logged = signal(false)
  currentStep = signal<'step1' | 'step2' | 'step3' | null>(null)

  signIn() {
    this.logged.set(true)
  }

  logout() {
    this.logged.set(false)
  }

  render() {
    console.log('render compo2')
  }
}
