import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, signal } from '@angular/core';
import { takeUntilDestroyed, toObservable, toSignal } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { Theme } from 'daisyui';
import { delay, distinctUntilChanged, interval, map, mergeMap, pairwise, startWith, tap } from 'rxjs';
import { ThemeService } from '../core/theme.service';

@Component({
  selector: 'app-demo10-rxjs-signal',
  standalone: true,
  imports: [
    AsyncPipe,
    JsonPipe,
  ],
  template: `
    <p>
      demo10-rxjs-signal works! {{total()}}
    </p>
    
    <pre>{{usersSignal() | json}}</pre>
    <button (click)="themeSrv.value.set('dark')">dark</button>
    <button (click)="themeSrv.value.set('light')">light</button>
  
  `,
  styles: ``
})
export default class Demo10RxjsSignalComponent {
  usersSignal = toSignal(
    inject(HttpClient)
      .get<User[]>('https://jsonplaceholder.typicode.com/users')
      .pipe(
        map(users => users.map(u => u.name)),
        delay(2000),
      )
  )

  total = computed(() => this.usersSignal()?.length)

  constructor(public themeSrv: ThemeService) {
    toObservable(themeSrv.value)
      .pipe(
        startWith(''),
        distinctUntilChanged(),
        pairwise(),
      )
      .subscribe(([prev, current]) => {
        console.log(prev, current)
      })


  }
}

type User = {
  id: number
  name: string;
}
