import { CommonModule } from '@angular/common';
import { Component, inject, Type, ViewContainerRef } from '@angular/core';
import { ProductsService } from '../core/products.service';
import { CardComponent } from '../shared/components/card.component';
import { TabBarComponent } from '../shared/components/tab-bar.component';
import { Child1Component } from './components/child1.component';
import { Child2Component } from './components/child2.component';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [CommonModule, Child2Component, Child1Component],
  template: `
      
    <h1>Demo COmponent Outlet</h1>

    <ng-template
        *ngComponentOutlet="compo.type;  inputs: compo.data"
    ></ng-template>
      
    <button class="btn" (click)="load1()">load tabbar component</button>
    <button class="btn" (click)="load2()">load card component</button>
  `,
  styles: ``,
})
export default class Demo6Component {
  view = inject(ViewContainerRef);

  compo: CompoConfig = {
    type: CardComponent,
    data: {
      counter: 123,
      zoom: 10
    }
  }

  load1() {
    this.compo = {
      type: TabBarComponent,
      data: {
        items: [5, 6, 7]
      }
    }
  }

  load2() {
    this.compo = {
      type: CardComponent,
      data: {
        counter: 123,
        zoom: 10
      }
    }
  }

 /* ngOnInit() {
    const ref = this.view.createComponent(CardComponent)
    /!*ref.instance.value = 1
    ref.instance.zoom = 2*!/
    // angular 14
    ref.setInput('counter', 11)
    ref.setInput('zoom', 22)

  }*/
}


export type CompoConfig = {
  type: Type<CardComponent | TabBarComponent>,
  data: any;
}
