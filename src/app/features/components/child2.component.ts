import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-child2',
  changeDetection: ChangeDetectionStrategy.OnPush,

  standalone: true,
  imports: [],
  template: `
    <p>
      child2 works!{{themeSrv.value()}}
    </p>
  `,
  styles: ``
})
export class Child2Component {
  themeSrv = inject(ThemeService)
}
