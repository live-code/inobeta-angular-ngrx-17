import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-child1',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
  template: `
    <p (click)="themeSrv.value.set('light')">
      child1 works! {{themeSrv.value()}}
    </p>
  `,
  styles: ``
})
export class Child1Component {
  themeSrv = inject(ThemeService)
}
