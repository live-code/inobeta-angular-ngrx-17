import { provideHttpClient } from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { provideStoreDevtools } from '@ngrx/store-devtools';

import { routes } from './app.routes';
import { provideState, provideStore } from '@ngrx/store';
import { cartFeature } from './core/store/cart/cart.feature';
import { counterFeature } from './core/store/counter/counter.feature';
import { provideEffects } from '@ngrx/effects';
import * as productsEffects from './core/store/products/products/products.effects';
import { provideRouterStore, routerReducer } from '@ngrx/router-store';
import * as routerEffects from './core/store/router/router.effects';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withComponentInputBinding()),
    provideHttpClient(),
    provideStore(),
    provideState({ name: counterFeature.name, reducer: counterFeature.reducer }),
    provideState({ name: cartFeature.name, reducer: cartFeature.reducer }),
    provideState({ name: 'todos', reducer: () => 123 }),
    provideState({ name: 'router', reducer: routerReducer }),
    // provideState({ name: productsFeature.name, reducer: productsFeature.reducer }),
    // provideState({ name: filtersFeature.name, reducer: filtersFeature.reducer }),
    // provideStoreDevtools({ maxAge: 10 }),
    provideStoreDevtools({ maxAge: 10, trace: true , connectInZone: true}),
    provideEffects([productsEffects, routerEffects]),
]
};
