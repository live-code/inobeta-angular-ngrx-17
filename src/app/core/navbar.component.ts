import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { Store } from '@ngrx/store';
import { MyRouterLinkActiveDirective } from '../shared/directives/my-router-link-active.directive';
import { SharedModule } from '../shared/shared.module';
import { selectList, selectTotalCost, selectTotalProductsInCart } from './store/cart/cart.feature';
import { selectUrl } from './store/router/router.selectors';
import { ThemeService } from './theme.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    RouterLink,
    SharedModule,
    RouterLinkActive,
    MyRouterLinkActiveDirective
  ],
  template: `
      {{themeSrv.isDark()}}
      
      <div class="flex  flex-wrap gap-3">
        <button class="btn" routerLinkActive="btn-info" routerLink="demo1">demo1</button>
        <button class="btn" routerLinkActive="btn-info" routerLink="demo2">demo2</button>
        <button class="btn" routerLinkActive="btn-info" routerLink="demo3">demo3</button>
        <button class="btn" routerLinkActive="btn-info" routerLink="demo4">demo4</button>
        <button class="btn" routerLinkActive="btn-info" routerLink="demo5">demo5</button>
        <button class="btn" routerLinkActive="btn-info" routerLink="demo6">demo6</button>
        <button class="btn" routerLinkActive="btn-info" routerLink="demo7-ngrx">demo7-ngrx</button>
        <button class="btn" routerLinkActive="btn-info" routerLink="demo8-ngrx-async">demo8-ngrx-async</button>
        <button class="btn"
                routerLink="demo9"
                appMyRouterLinkActive="btn-info">demo9 directive</button>
          <button class="btn" routerLinkActive="btn-info" routerLink="demo10-rxjs-signal">demo10-rxjs-signal</button>
          <button class="btn" routerLinkActive="btn-info" routerLink="demo11-uikit">demo11-uikit</button>
          
          CART: {{totalCartItems()}} - (€ {{totalCost()}}) - {{url()}}
      </div>
  `,
  styles: ``,
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent {
  themeSrv = inject(ThemeService)
  store = inject(Store)
  cart = this.store.selectSignal(selectList)
  totalCartItems = this.store.selectSignal(selectTotalProductsInCart)
  totalCost = this.store.selectSignal(selectTotalCost)
  url = this.store.selectSignal(selectUrl)

}
