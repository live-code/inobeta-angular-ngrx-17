import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, exhaustMap, map, mergeMap, of, switchMap, tap, withLatestFrom } from 'rxjs';
import { Product } from '../../../../model/product';
import { RouterActions } from '../../router/router.actions';
import { ProductsActions } from './products.actions';

export const loadProducts = createEffect((
  actions$ = inject(Actions),
  http = inject(HttpClient)
) => {
  return actions$.pipe(
    ofType(ProductsActions.load),
    switchMap(
      () => http.get<Product[]>('http://localhost:3000/products')
        .pipe(
          map((items) => ProductsActions.loadSuccess({ items })),
          catchError(error => of(ProductsActions.loadFail()))
        )
    )
  )
}, { functional: true })

export const loadProductsSuccess = createEffect((
  actions$ = inject(Actions),
  http = inject(HttpClient)
) => {
  return actions$.pipe(
    ofType(ProductsActions.loadSuccess),
    map(() => RouterActions.go({ path: 'demo2'}))
  )
}, { functional: true })

