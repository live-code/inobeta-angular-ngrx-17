
// custom selector
import { createSelector } from '@ngrx/store';
import { selectText } from './filters.feature';
import { selectList } from '../products/products.feature';

export const selectFilteredList = createSelector(
  selectList,
  selectText,
  (list, text) => list.filter(item => item.name.toLowerCase()
    .includes(text.toLowerCase()))
)

