// core/store/router/router.actions.ts
import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';

export const RouterActions = createActionGroup({
  source: 'Router',
  events: {
    'go': props<{ path: string }>(),
    'back': emptyProps,
    'forward': emptyProps,
  }
})

// export const SyncStorage = createAction('sync storage')


/*
// OLD WAY
export const go = createAction(
  '[ROUTER] go',
  props<{ path: string }>()
);

export const back = createAction('[ROUTER] back');
export const forward = createAction('[ROUTER] forward');
*/
