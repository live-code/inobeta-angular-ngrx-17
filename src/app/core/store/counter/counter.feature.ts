import { props, createAction, createFeature, createReducer, createSelector, on } from '@ngrx/store';

export const increment = createAction('[counter] increment')
// export const decrement = createAction<number>('decrement')
export const decrement = createAction('[counter] decrement', props<{value: number}>())

export const counterFeature = createFeature({
  name: 'counter',
  reducer: createReducer(
    { value: 0, multipler: 5},
    on(increment, (state, action) => ({ ...state, value: state.value + 1})),
    on(decrement, (state, action) => ({ ...state, value: state.value - action.value}))
  )
})

export const {
  selectCounterState,
  selectValue,
  selectMultipler
} = counterFeature


export const selectValueMultiplied = createSelector(
  selectValue,
  selectMultipler,
  (value, multiplier) => value * multiplier
)
