import { computed, effect, Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  value = signal<'dark' | 'light'>('dark');
  isDark = computed(() => this.value() === 'dark')
}

